import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index.js'

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'

import FontAwesomeIcon from "./fontawesome/index.js";

createApp(App)
.use(router)
.component('font-awesome-icon', FontAwesomeIcon)
.mount('#app')
